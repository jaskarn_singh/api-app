
// Leaflet map key code

// Map initial positioning

var mymap = L.map('mapid').setView([-37.6878, 176.1651], 12.5);

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1IjoiamFza2FybnNpbmdoIiwiYSI6ImNqdmVoOTFkNjB3MTE0NG9kcm5oNjhsaGoifQ.r7bwns6dGQHcXTxwfUtbUw'
}).addTo(mymap);

// GEOCODE API key - Local: Y4fhb3ETlqPbEaBOVvHNk5ojqmDUAx3F
// GEOCODE API key - Waikato : 5Xsds6Zjnc2dWP2oroGcU3gYwbiIO1pt

// Heading Div Element & Creation

head = document.createElement("h2");
head.innerHTML = "New Zealand Tourist Information";


heading.appendChild(head);

// Input Div Elements & Creation

var info = document.createElement ("div");
info.label = document.createElement("div");
info.label.innerHTML = "What Town would you like to visit?";

// Input Textbox Element & Creation

info.search = document.createElement("input");
info.search.setAttribute("type", "text");
info.search.setAttribute("id", "inputbox");

// Input Button Element & Creation

info.button = document.createElement("button");
info.button.setAttribute("type", "button");
info.button.innerHTML= "Submit";
info.button.onclick = function(){
    getSearch();
}

info.appendChild(info.label);
info.appendChild(info.search);
info.appendChild(info.button);
Input.appendChild(info);

// Recent Searches Div Element & Creation

var search = document.createElement("div");
search.label = document.createElement("div");
search.label.innerHTML = "Recent Searches - Click on Cities to reload Information";

search.appendChild(search.label);
searchItems.appendChild(search);

// Sunrise and Sunset Div Element & Creation

var setrise = document.createElement("div");
setrise.label = document.createElement("div");
setrise.label.innerHTML = "Sunrise & Sunset";

setrise.appendChild(setrise.label);
riseset.appendChild(setrise);

// Weather Div Element & Creation

var we = document.createElement("div");
we.label = document.createElement("div");
we.label.innerHTML = "Current Weather";

we.appendChild(we.label);
weather.appendChild(we);

// Weather Div Temp Element & Creation
var wea = document.createElement("div");
wea.label = document.createElement("div");
wea.label.innerHTML = "Current Temp";

wea.appendChild(wea.label);
weather1.appendChild(wea);

// getSearch Function - Shows location of user input

var getSearch = function(){
    var citysearch = document.getElementById("inputbox").value;
    console.log (citysearch);

    // Fetch to gather information from the API

    fetch("https://www.mapquestapi.com/geocoding/v1/address?key=5Xsds6Zjnc2dWP2oroGcU3gYwbiIO1pt&location=" +citysearch+ ",nz")
    .then(response => response.json())
    .then(json => a(json));

    // Function to show the user input based on lat, lng returned from the fetch

    a = function(json){
       // console.log(json.results[0].locations[0]);
        var location = json.results[0].locations[0];
        //var latlng = location.latLng;
        //console.log(latlng);
        var lat = location.latLng.lat;
        var lng = location.latLng.lng;
        var city = location.adminArea5;
        console.log(lat, lng);
        mymap.setView([lat, lng], 12.5);
        getSun(lat, lng);
        getWeather(city);
        recent(lat, lng, city);
    }  
}

// Get Sunrise & Sunset Function - Uses user input and lat and lng values to show sunrise and sunset of city

getSun = function(lat, lng){
    console.log(lat,lng);

    // fetch is used again using lat and lng to provide us information from the API

    fetch("map.php?lat=" +lat+ "&lng=" +lng)
    .then(response => response.json())
    .then(json => showSun(json));
}

// Main function to show the sunrise an sunset and place it in the correct div

showSun = function(info){
    city = document.getElementById("inputbox").value;
    var sunrise = info.results.sunrise;
    var sunset = info.results.sunset;
    div = document.getElementById("riseset");
    div.innerHTML = city + " at this moment: Sun sets at " +sunrise+ " and rises at " +sunset;
}

// Get Weather Function - Shows weather of city based on user input

//api key = 9dd1233ec41ede694e92d8303d2b1e50

getWeather = function(city){
    
    // XMLHTTpRequest as we are pulling xml from the API

    request = new XMLHttpRequest();
    url = "weather.php?town=" + city;
	request.open("GET",url);
	request.onreadystatechange = function(){
		if(request.readyState == 4) {
    		if (request.status == 200) {
    			result = request.responseText; 
    			showWeather(result);
			}
		}
	};
	request.send();
}

// Data is then parsed as xml then displayed to the required div

showWeather = function(result){

    let parser = new DOMParser();
    
    xmlDoc = parser.parseFromString(result,"text/xml");
    
    we = xmlDoc.getElementsByTagName("weather")[0];
    temp = xmlDoc.getElementsByTagName("temperature")[0];
    prec = xmlDoc.getElementsByTagName("precipitation")[0];

    display = "Current Weather is " + we.getAttribute('value');
    display1 = " Min Temperature is " +temp.getAttribute('min') + ", Max Temperature is " +temp.getAttribute('max');
    document.getElementById("weather").innerHTML = display;
    document.getElementById("weather1").innerHTML = display1;

    // Two divs just for to keep the weather information display tidy
}


// RecentSearch Function - Stores all user input for easier reuse

// Initializes a object to store all user input values - cities

var recent = function(lat, lng, city){
    
    var lat = lat;
    var lng = lng;
    var city = city;

    var _ui = {
        city_label: null,
        dom:null,
    }

    // Search elements creation

    //onclick button allowing user to click the results to change the information back to the previous cities

    _ui.dom = document.createElement("div");
    _ui.city_label = document.createElement("span");
    _ui.city_label.innerHTML = city;
    _ui.city_label.onclick = function(){
        document.getElementById("inputbox").value = city;
        getSun(lat, lng);
        getWeather(city);
        mymap.setView([lat, lng], 12.5);
    }

    _ui.dom.appendChild(_ui.city_label);
    searchItems.appendChild(_ui.dom);
}

